<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Insert a Book Name </title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="style.css" rel="stylesheet">
  </head>
<body>

<form class="form-group-sm">
  
  <div class="form-group form-group-sm">
  
    <label class="col-sm-12 control-label" for="formGroupInputSmall">Insert a Book Name: </label>
    
      
    </div>
  </div>
</form>

<br><br>
<form class="form-group-sm" method="POST" action="store.php">
  
  <div class="form-group form-group-sm">
  
    <label class="col-sm-1 control-label" for="formGroupInputSmall">Book Name: </label>
    <div class="col-sm-6">
      
        <input class="form-control" type="text" id="formGroupInputSmall" placeholder="name here" name="title">
      <button type="submit" class="btn btn-primary btn-sm">Add</button>
    </div>
  </div>
</form>


<p>
 
  <a class = "btn btn-info "href="index.php">List</a>
</p>


	
	
	
	

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
	<?php
      
      
      ?>
  </body>
</html>