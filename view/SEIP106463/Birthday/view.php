<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Birth Date</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="style.css" rel="stylesheet">

  </head>
  <body>
  
<div class="container">
  <h2>View Date</h2>                           
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>ID</th>
        <th>Date</th>
		<th>Actions</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1</td>
        <td>1993-07-16</td>
			<td>   
		    <a class="btn" href="#">View Details</a>	
            <a class="btn" href="#">Edit</a>
	        <a class="btn" href="#">Delete</a>
	     </td>
      </tr>


    </tbody>
  </table>
  
  <a class = "btn btn-info "href="index.php">Back</a>
  <a class = "btn btn-info "href="create.php">Create New</a>


    




    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <?php
      include '../../../src/BITM/SEIP106463/Birthday/date.php';
      $my_birthday= new date();
        $my_birthday-> index();
      
      ?>
  </body>
</html>