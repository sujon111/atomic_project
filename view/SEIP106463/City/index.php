<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>City</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="style.css" rel="stylesheet">

  </head>
  <body>
       <?php
      include_once '../../../vendor/autoload.php';
      use App\BITM\SEIP106463\City\city;
      $my_city= new city();
      $citys=$my_city->index();
      
      ?>
  
<div class="container">
  <h2>List of User</h2>                           
  <table class="table table-bordered">
      <a class = "btn btn-info "href="create.php">Create New</a>
    <thead>
      <tr>
        <th>User</th>
        <th>Name</th>
        <th>City</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
        <?php
        foreach ($citys as $city){
        ?>
      <tr>
          <td><?php echo $city['ID'] ?></td>
        <td><?php echo $city['Name'] ?></td>
        <td><?php echo $city['City'] ?></td>
        
            <td>   
		    <a class="btn" href="view">View</a>	
            <a class="btn" href="edit.php">Edit</a>
	        <a class="btn" href="delete.php">Delete</a>
	     </td>
      </tr>
      <?php
        }
      
      ?>
      <tr>
        
    </tbody>
  </table>
  
  
  <label class="col-sm-6 control-label" for="formGroupInputSmall"></label>
    <label class="col-sm-6 control-label" for="formGroupInputSmall">
	<nav>
  <ul class="pagination">
    <li>
      <a href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>
    <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li><a href="#">4</a></li>
    <li><a href="#">5</a></li>
    <li>
      <a href="#" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
    </li>
	
  </ul>
  
</nav>

	</label>

    




    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
   
  </body>
</html>