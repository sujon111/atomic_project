<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Summary of organization</title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="container">

            <h3>Summary List</h3>
            <a href="create.php" class="btn btn-default bg-success ">Add new summary</a>
            <hr>
            <table class="table table-bordered table-hover text-center bg-info">
                <thead >
                    <tr>
                        <th class="text-center">ID</th>
                        <th class="text-center">Summary</th>
                        <th colspan="3" class="text-center">Action</th>
                        
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1.</td>
                        <td>lorem ipsum dolor...</td>
                        <td><a href="view.php" class="btn btn-default ">View</a></td>
                        <td><a href="edit.php" class="btn btn-default ">edit</a></td>
                        <td><a href="delete.php" class="btn btn-default ">delete</a></td>
                    </tr>
                    <tr>
                        <td>2.</td>
                        <td>lorem ipsum dolor...</td>
                        <td><a href="view.php" class="btn btn-default ">View</a></td>
                        <td><a href="edit.php" class="btn btn-default ">edit</a></td>
                        <td><a href="delete.php" class="btn btn-default ">delete</a></td>
                    </tr>
                    <tr>
                         <td>3.</td>
                        <td>lorem ipsum dolor...</td>
                        <td><a href="view.php" class="btn btn-default ">View</a></td>
                        <td><a href="edit.php" class="btn btn-default ">edit</a></td>
                        <td><a href="delete.php" class="btn btn-default ">delete</a></td>
                    </tr>
                </tbody>
            </table>

        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
        <?php
      include '../../../src/BITM/SEIP106463/summary/summary.php';
      $my_summary= new summary();
        $my_summary-> index();
      
      ?>
    </body>
</html>